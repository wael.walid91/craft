var fs = require('fs');
const path = require('path');
const config = require("../src/config")
import chalk from 'chalk';
export class BaseGeneration {
    constructor(options){
        this.options = options 
    }
    removingSpecialChars(){
        return this.options.componentName.replace(/[^a-zA-Z ]/g, " ").toLowerCase();
    }
    createIfNotExistDirectory(newFilePath){
        if (!fs.existsSync(newFilePath)){
            this.mkDirByPathSync(newFilePath);
            console.log(chalk.green.bold(`DONE :: Creating Directory -> ${config.paths.displayPath(newFilePath)}`));
        }
    }
    async cloneFile(filePath , newFilePath , fileName , extenstion){
        
        /** check if the file already exist  */
        if (fs.existsSync(newFilePath+"/"+fileName+extenstion)){
            
            console.log(chalk.yellow.bold(`Already Exist!:: ${config.paths.displayPath(newFilePath)+"/"+fileName+extenstion}`));
            return false ;
        }
        this.createIfNotExistDirectory(newFilePath);
      
        fs.readFile(filePath, 'utf8', function (err,data) {
        if (err) {
            return console.log(err);
        }
        var result = data.replace(/FILENAME/g, fileName);
        
            fs.writeFile(newFilePath+"/"+fileName+extenstion, result, 'utf8', function (err) {
                if (err) return console.log(err);
                
                console.log(chalk.green.bold(`Successfully Created : ${config.paths.displayPath(newFilePath)}/${fileName+extenstion}!`))
            });
        });
    }

    mkDirByPathSync(targetDir, { isRelativeToScript = false } = {}) {
        const sep = path.sep;
        const initDir = path.isAbsolute(targetDir) ? sep : '';
        const baseDir = isRelativeToScript ? __dirname : '.';
      
        return targetDir.split(sep).reduce((parentDir, childDir) => {
          const curDir = path.resolve(baseDir, parentDir, childDir);
          try {
            fs.mkdirSync(curDir);
          } catch (err) {
            if (err.code === 'EEXIST') { // curDir already exists!
              return curDir;
            }
      
            // To avoid `EISDIR` error on Mac and `EACCES`-->`ENOENT` and `EPERM` on Windows.
            if (err.code === 'ENOENT') { // Throw the original parentDir error on curDir `ENOENT` failure.
              throw new Error(`EACCES: permission denied, mkdir '${parentDir}'`);
            }
      
            const caughtErr = ['EACCES', 'EPERM', 'EISDIR'].indexOf(err.code) > -1;
            if (!caughtErr || caughtErr && curDir === path.resolve(targetDir)) {
              throw err; // Throw if it's just the last created dir.
            }
          }
      
          return curDir;
        }, initDir);
      }

    capitalize(string){
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
      
}