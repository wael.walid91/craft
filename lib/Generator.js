import ServiceGenerator from "./ServiceGenerator"
import GenericFileGenerator from "./GenericFileGenerator"
export default class Generator{
    constructor(options){
        this.options = options ; 
    }
    generate(){
        return new GenericFileGenerator(this.options).sendOut();
    }
}