import { BaseGeneration } from "./BaseGeneration";
var serviceDump = require("../templates/service-dump");
const formInputs = require("../templates/service-inputs.json");
const config = require("../src/config");
export default class GenericFileGenerator extends BaseGeneration{

    constructor(options){
        super(options);
        this.files = {
            request : this.options.request ,
            model : this.options.model,
            util : this.options.util,
            service : this.options.service,
            requestHandler : this.options.requestHandler,
            posHandler : this.options.posHandler
        }
        ;
        this.fileName = "" ;
        this.formatingFilename() ;
        this.handlingFiles() ;
    }
    formatingFilename(){
        
        this.fileName = this.removingSpecialChars();
         
        this.fileName =  this.fileName.toLowerCase().split(' ').map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join('');
    }
    handlingFiles(){
        this.files = Object.keys(this.files).filter(element => this.files[element]);
        if(this.options.component != "newService" && !this.files.includes(this.options.component)){
            this.files.push(this.options.component) ; 
        }
    }
    makeJSFile(){
    
        // console.log(this.files)
        
        this.files.forEach(file => {
            /** forTesting */
        // config[file].path = config.paths.temp ;
            
            let completeFileName = this.fileName+this.capitalize(file);
            this.cloneFile(`${config.paths.templates}/${config[file].template}` , `${config[file].path}` , completeFileName , config[file].extention);
            
            
        });
        
    }

    sendOut(){
        return this.makeJSFile() ;
        return this.files;
    }
}