import { BaseGeneration } from "./BaseGeneration";
var serviceDump = require("../templates/service-dump");
const formInputs = require("../templates/service-inputs.json");
export default class ServiceGenerator extends BaseGeneration{
    constructor(options){
        super(options);
        this.files = [] ;
        this.fileName = "" ;
    }
    formatingFilename(){
        
        this.fileName = this.removingSpecialChars();
        this.fileName = this.fileName.toLowerCase().split(" ").join(".");
        return this.fileName ;
        
    //     switch(options.component) {
    //         case "newService":
    //         fileName = fileName.toLowerCase().split(" ").join(".");
    //         break;
    //         default:
    //         fileName = fileName.toLowerCase()
    //         .split(' ')
    //         .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
    //         .join(' ');
    //         }
    //         console.log(options)
    //         return fileName + config.naming[options.component].toUpperCase() ;
    }

    mappingInputs(){
       /** checking choosen inputs */
        if(this.options.componentOption.inputs && this.options.componentOption.inputs.length){
            var requiredInputs = [] ;
            let optionInputs = this.options.componentOption.inputs ; 
            /** pinding choosen inputs */
            optionInputs.forEach(e => {
                let input = formInputs.find(element => element.template === e)
                requiredInputs.push(input)
            });     
        }
        return requiredInputs || false ;
    }
 
    handlingHandlers(){
        if(this.options.componentOption.handlers && this.options.componentOption.handlers.length){
            var requiredInputs = [] ;
            let optionInputs = this.options.componentOption.inputs ; 
            /** pinding choosen inputs */
            optionInputs.forEach(e => {
                let input = formInputs.find(element => element.template === e)
                requiredInputs.push(input)
            });     
        }
        return requiredInputs || false ;
        
    }

    parseService(){

    }


    sendOut(){
        
        return this.handlingHandlers()
    }
}