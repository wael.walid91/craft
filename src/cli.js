import arg from 'arg';
import inquirer from 'inquirer';
import { doCommand } from './main';
import { confirmAnswerValidator, confirmChoiceValidator } from "./validators";
const avaliableTemplates = {
  newService: {
    options: {
      handlerName: "//middlestring//",
      inputs: {},
      path: "config/services/",
      extension: ".json",

    },
    questions: [
      {
        type: 'checkbox',
        name: 'handlers',
        message: 'Please choose handlers to create.',
        choices: ['orderSummaryHandler', 'quoteHandler', "purchaseHandler", "requeryHandler"],
        default: [],
        // validate:confirmChoiceValidator
      },
      {
        type: 'checkbox',
        name: 'inputs',
        message: 'Please choose inputs to generate with the new service.',
        choices: ['phone_with_country',
          'money',
          'text',
          'number',
          'plans',
          'list',
          'authentication',
          'message',
          'message',
          'message_error',
          'stop',
          'data_collections_user_v2',
          'webview',
          'detailed_offers',
          'slider',
          'date',
          'loading',
          'otp',
          'file_upload',
          'pin',
          'pin',
          'dynamic_list'],
        default: [ 
        'phone_with_country',
        'money',
        'plans' ],
        // validate:confirmChoiceValidator
      },
      {
        type: 'confirm',
        name: 'definitions',
        message: 'Do you want to create definitions property ?',
        default: true
      }
    ]
  },
  request: 'request',
  model: 'model',
  util: 'util',
  service: 'service',
  requestHandler: 'requestHandler',
  posHandler: 'posHandler'
};

const templatesArray = Object.keys(avaliableTemplates);
const defaultTemplate = templatesArray[0];




function parseArgumentsIntoOptions(rawArgs) {
  const args = arg(
    {
      '-r': Boolean, /** request */
      '-m': Boolean, /** model */
      '-u': Boolean, /** util */
      '-s': Boolean, /** service */
      '-p': Boolean,
      '-h' : Boolean

    },
    {
      argv: rawArgs.slice(2),
    }
  );

  return {
    component: args._[0],
    componentName: args._[1],
    request: args['-r'] || false,
    model: args['-m'] || false,
    util: args['-u'] || false,
    service: args['-s'] || false,
    requestHandler: args['-h'] || false,
    posHandler: args['-p'] || false
  };
}



async function promptForMissingOptions(options) {

  var questions = [];

  if (!options.componentName) {
    questions.push({
      type: 'input',
      name: 'componentName',
      message: 'Please provide the file name?',
      validate: confirmAnswerValidator
    });
  }
  if (!options.component || !templatesArray.includes(options.component)) {
    questions.push({
      type: 'list',
      name: 'component',
      message: 'Please choose which component to use',
      choices: templatesArray,
      default: defaultTemplate,
      //  validate:confirmChoiceValidator
    });
  }

  var answers = await inquirer.prompt(questions);
  questions = [];
  if (
    (avaliableTemplates[answers["component"]] && avaliableTemplates[answers["component"]].questions)
    ||
    (avaliableTemplates[options.component] && avaliableTemplates[options.component].questions)
  ) {
    // remove the first question if exist 
    // questions.splice(0, 1);
    questions = avaliableTemplates[answers['component'] || options.component].questions;
  }
  //    console.log(questions);



  //    if (!options.componentName) {
  //     questions.push({
  //       type: 'input',
  //       name: 'componentName',
  //       message: 'Please provide the file name?',
  //       validate:confirmAnswerValidator
  //     });
  //   }

  //    if (!options.model) {
  //     questions.push({
  //       type: 'confirm',
  //       name: 'model',
  //       message: 'Do you want to create model with this request?',
  //       default: false,
  //     });
  //   }



  answers = { ...answers, ...await inquirer.prompt(questions) };
  //  console.log(answers) ;
  return {
    ...options,
    component: options.component || answers.component,
    componentName: options.componentName || answers.componentName,
    componentOption: answers
  };
}

export async function cli(args) {
  // console.log(args) ;
  let options = parseArgumentsIntoOptions(args);
  options = await promptForMissingOptions(options);
  await doCommand(options);
}