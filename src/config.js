const path = require("path") ; 
const basePath = path.join(__dirname, "../../../");
const appPath = path.join(__dirname,"../");
module.exports =  {

    paths : {
        appPath: appPath ,
        basePath : basePath,
        newService : basePath+"config/services",
        handlerPath : basePath+"handlers",
        service : basePath+"services",
        request : basePath+"client/requests",
        model : basePath+"client/models",
        util : basePath+"client/utils",
        temp : appPath+"temp",
        templates : appPath+"templates",
        displayPath : function(path){
            return path.replace(basePath,"");
        }
    },
    naming : {
        newService : "" ,
        handler : "Handler",
        service: "Service",
        request: "Request",
        model: "Model",
        util: "Util",
        route: "Route"
    },
    posHandler:{
        template : "posHandler.js" ,
        path : basePath+"handlers" ,
        extention : ".js"
    },
    requestHandler:{
        template : "requestHandler.js" ,
        path : basePath+"handlers" ,
        extention : ".js"
    },
    request : {
        template : "clientRequest.js" ,
        path : basePath+"client/requests",
        extention : ".js"
    },
    model : {
        template : "clientModel.js" ,
        path : basePath+"client/models",
        extention : ".js"
    },
    util : {
        template : "clientUtil.js" ,
        path : basePath+"client/utils",
        extention : ".js"
    },
    service : {
        template : "adapterService.js" ,
        path : basePath+"services",
        extention : ".js"
    }
}