
export const confirmAnswerValidator = async (input) => {
    if (!input) {
        return 'Please provide as Answer';
    }
    return true ;
};

export const confirmChoiceValidator = async (input , templatesArray) => {
    if (!templatesArray.includes(input)) {
        return 'Please choose right component!';
    }
    return true ;
};