const Joi = require('joi');
const config = require('../../config/config.json');

module.exports = class FILENAME {
  constructor({ email } = {}) {
    this.config = config;
    this.email = email;
  }
  schemaValidation() {
    return {
      email: Joi.string()
        .email()
        .required(),
    };
  }
  dataValidation() {
    return {
      email: this.email,
    };
  }
};
