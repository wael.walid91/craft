const BaseApiClient = require("./BaseApiClient");
const config = require("../../config/config.json");
const userStatusFlow = require("../../data/userStatus.json")

module.exports = class FILENAME extends BaseApiClient {
  constructor(requestModel, customConfig) {
    super(requestModel, customConfig);
    this.url = "" ;
    this.config = config;
    this.method = '';
  }
  
  async _request() {
    return new Promise(async (resolve, reject) => {
    try {
      // validate basic properties
      super._validate();
      // make request and wait response
      let body = await super._makeRequest();
      return resolve(body);

    } catch (e) {
      console.log(
        `${new Date().toISOString()} Classname .FunctionName() error : ${JSON.stringify(
          e.message || e
        )}`
      );
      return reject(e);
    }
    });
  }

  async result() {
      /** request output */
      result = "" ;
      return this._parseResult(result)
  }
  _parseResult(result){
    /** manipulation of the response */
  }
  
 
};
