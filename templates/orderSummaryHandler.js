const OrderSummaryResponse = require('./../models/OrderSumaryResponse');
const PaymentDetailItem = require('./../models/PaymentDetailItem');
const AdditionalDetailItem = require('./../models/AdditionalDetailItem');
const AppError = require('../models/AppError');
const ResponseCode = require('../models/ResponseCode');
const config = require('../config/config.json');
const availableServices = require('../config/requireServices').services;

module.exports = {
  orderSummaryHandler(serviceKey, body) {
    return new Promise(async (resolve, reject) => {
      try {
        // const orderSummaryResponse = new OrderSummaryResponse(
        //     destination,
        //     additionalDetails,
        //     paymentDetails,
        // );
        // return resolve(orderSummaryResponse);
      } catch (error) {
        const errorText = `${serviceKey} purchasehandler failed with error => ${error.toString()}${JSON.stringify(
            error,
        )}`;
        console.log(errorText);
        if (error instanceof AppError) {
          return reject(error);
        }
        return reject(new AppError(400, ResponseCode.FALIED, errorText, [error]));
      }
    });
  },
};
