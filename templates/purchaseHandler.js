const PurchaseResponse = require('../models/PurchaseResponse');
const AppError = require('../models/AppError');
const ResponseCode = require('../models/ResponseCode');
const config = require('../config/config.json');
const availableServices = require('../config/requireServices').services;

module.exports = {
  purchaseHandler(serviceKey, body) {
    return new Promise(async (resolve, reject) => {
      try {
        
        // const makePurchaseResponse = new PurchaseResponse(
        //   transaction_reference,
        //   transaction_code,
        //   response,
        //   '',
        // );

        // return resolve(makePurchaseResponse);
      } catch (error) {
        const errorText = `${serviceKey} purchasehandler failed with error => ${error.toString()}${JSON.stringify(
          error,
        )}`;
        console.log(errorText);

        if (error instanceof AppError) {
          return reject(error);
        }
        return reject(new AppError(200, ResponseCode.PENDING, errorText, [error]));
      }
    });
  },
};
