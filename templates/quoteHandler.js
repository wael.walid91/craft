const QuoteResponse = require('../models/QuoteResponse');
const AppError = require('../models/AppError');
const ResponseCode = require('../models/ResponseCode');
const config = require('../config/config.json');
const availableServices = require('../config/requireServices').services;

module.exports = {
  quoteHandler(serviceKey, body) {
    return new Promise(async (resolve, reject) => {
      try {

        // const manifestPriceComponents = [
        //   { name: 'manifist_initial_price', value: amount },
        //   { name: 'manifist_fee', value: 0.0 },
        // ];

        // const quoteResponse = new QuoteResponse(
        //   generatedReference,
        //   availableServices[serviceKey].destination,
        //   {
    
        //   },
        //   manifestPriceComponents,
        //   currency
        // );
        // return resolve(quoteResponse);
      } catch (error) {
        const errorText = `${serviceKey} quoteHandler failed with error => ${error.toString()}${JSON.stringify(
          error,
        )}`;
        console.log(errorText);

        if (error instanceof AppError) {
          return reject(error);
        }
        return reject(new AppError(400, ResponseCode.PENDING, errorText, [error]));
      }
    });
  },
};
