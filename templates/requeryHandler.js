const PurchaseResponse = require('../models/PurchaseResponse');

const AppError = require('../models/AppError');
const BodyService = require('../services/bodyService');
const ResponseCode = require('../models/ResponseCode');

module.exports = {
  requeryHandler(serviceKey, body) {
    return new Promise(async (resolve, reject) => {
      try {
        // const requeryResponse = new PurchaseResponse(
        //   transaction_reference,
        //   transaction_reference,
        //   response,
        //   '',
        // );

        // return resolve(requeryResponse);
      } catch (error) {
        const errorText = `${serviceKey} requeryHandler failed with error => ${error.toString()}${JSON.stringify(
          error,
        )}`;
        console.log(errorText);

        if (error instanceof AppError) {
          return reject(error);
        }
        return reject(new AppError(200, ResponseCode.PENDING, errorText, [error]));
      }
    });
  },
};
