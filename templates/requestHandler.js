const AppError = require('../models/AppError');
const ResponseCode = require('../models/ResponseCode');

module.exports = {
  FILENAME: (formElements, serviceKey, body) => {
    return new Promise(async (resolve, reject) => {
      try {

        resolve(formElements);
      } catch (error) {
        console.log(
          `Requesthandler :: error while trying to fetch data:  ${JSON.stringify(
            error.message || error,
          )}`,
        );
        return reject(
          new AppError(
            500,
            ResponseCode.FAILED,
            `serviceRequesthandler Error getting data: ${JSON.stringify(error.message || error)}`,
            [error],
          ),
        );
      }
    });
  },
};
